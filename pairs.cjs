const obj = require("./objects.cjs");

function pairs(obj) {

    var keysString = keys(obj);
    var length = keysString.length;
    let arr = [];
    // console.log(pairs);
    for (var idx = 0; idx < length; idx++) {
        let temp = [keysString[idx], obj[keysString[idx]]];
        arr.push(temp);
    }
    return arr;

    
}

module.exports = pairs;