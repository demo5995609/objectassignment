const obj = require("./objects.cjs");

function invert(obj) {
    var result = {};
    var _keys = keys(obj);
    var  length = _keys.length;
    for (var idx = 0; idx < length; idx++) {
        result[obj[_keys[idx]]] = _keys[idx];
    }
    return result;
   
}

module.exports = invert;