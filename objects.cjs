const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

// Complete the following underscore functions.
// Reference http://underscorejs.org/ for examples.
// Check and use MDN as well
// PROJECT RESTRICTION: You can't use the built in Object methods.  Only use native JavaScript for loops. No other types of loops are allowed.
// PROJECT RESTRICTION: You can't use the underscore library or any other external library.

/*
    Create a function for each problem in a file called
        keys.cjs
        values.cjs
        pairs.cjs
    and so on in the root of the project.
    
    Ensure that the functions in each file is exported and tested in its own file called
        testKeys.cjs
        testValues.cjs
        testPairs.cjs
    and so on in a folder called test.

    Create a new git repo on gitlab for this project, ensure that you commit after you complete each problem in the project. 
    Ensure that the repo is a public repo.

    When you are done, send the gitlab url to your mentor
*/


function keys(obj) {

    // let arr = [];
    // for(objectKeys in obj){
    //     arr.push(objectKeys);
    // }
    // // console.log(arr);
    // return arr;
    
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys
}

function values(obj) {
    // let keysArr = keys(obj);
    // let valuesArr = [];
    // for(let idx = 0; idx<keysArr.length; idx++){
    //     // console.log(obj[keysArr[idx]]);
    //     valuesArr.push(obj[keysArr[idx]]);

    // }
    // return valuesArr;
    // let valuesString invert(testObject)= Object.values(obj);
    // return valuesString;
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
}



function mapObject(obj, cb) {
    // for(let key in obj){
    //     obj[key] = cb(obj[key]);
    // }
    // return obj;    
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
}

function pairs(obj) {

    var keysString = keys(obj);
    var length = keysString.length;
    let arr = [];
    // console.log(pairs);
    for (var idx = 0; idx < length; idx++) {
        let temp = [keysString[idx], obj[keysString[idx]]];
        arr.push(temp);
    }
    return arr;

    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
}

/* STRETCH PROBLEMS */

function invert(obj) {
    var result = {};
    var _keys = keys(obj);
    var  length = _keys.length;
    for (var idx = 0; idx < length; idx++) {
        result[obj[_keys[idx]]] = _keys[idx];
    }
    return result;
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert
}

function defaults(obj, defaultProps) {
    for(let key in defaultProps){
        // console.log(key);
        if(obj[key] != 'undefined'){
            obj[key] = defaultProps[key];
        }
    }
    return obj;
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
}

// console.log(defaults(testObject, {test:63}));
module.exports = testObject; 